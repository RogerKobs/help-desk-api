package br.edu.unisep.calls.domain.builder.tickets;

import br.edu.unisep.calls.data.entity.Tickets;
import br.edu.unisep.calls.domain.builder.TicketsBuilder;
import br.edu.unisep.calls.domain.dto.RegisterTicketsDto;
import br.edu.unisep.calls.domain.dto.UpdatedTicketsDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class TicketsBuilderTest {

    private TicketsBuilder ticketsBuilder;

    @BeforeEach
    void setup() {ticketsBuilder = new TicketsBuilder();}

    @Test
    void shouldCreateBookDto() {
        var ticket = new Tickets();

        ticket.setId(1);
        ticket.setTitle("Titulo de teste");
        ticket.setDescription("Descricao de teste");
        ticket.setNameIssuer("Roger");
        ticket.setEmailIssuer("roger@gmail.com");
        ticket.setNameResponder("Marco");
        ticket.setEmailResponder("marco@gmail.com");
        ticket.setStatus(1);

        var result = ticketsBuilder.from(ticket);

        assertEquals(ticket.getId(), result.getId());
        assertEquals(ticket.getTitle(), result.getTitle());
        assertEquals(ticket.getDescription(), result.getDescription());
        assertEquals(ticket.getNameIssuer(), result.getNameIssuer());
        assertEquals(ticket.getEmailIssuer(), result.getEmailIssuer());
        assertEquals(ticket.getNameResponder(), result.getNameResponder());
        assertEquals(ticket.getEmailResponder(), result.getEmailResponder());
        assertEquals(ticket.getStatus(), result.getStatus());
    }

    @Test
    void shouldCreateRegisterBookDto() {
        var registerTicket = new RegisterTicketsDto();

        registerTicket.setTitle("Titulo de teste");
        registerTicket.setDescription("Descricao de teste");
        registerTicket.setNameIssuer("Roger");
        registerTicket.setEmailIssuer("roger@gmail.com");
        registerTicket.setNameResponder("Marco");
        registerTicket.setEmailResponder("marco@gmail.com");

        var result = ticketsBuilder.from(registerTicket);

        assertNull(result.getId());
        assertEquals(registerTicket.getTitle(), result.getTitle());
        assertEquals(registerTicket.getDescription(), result.getDescription());
        assertEquals(registerTicket.getNameIssuer(), result.getNameIssuer());
        assertEquals(registerTicket.getEmailIssuer(), result.getEmailIssuer());
        assertEquals(registerTicket.getNameResponder(), result.getNameResponder());
        assertEquals(registerTicket.getEmailResponder(), result.getEmailResponder());
    }

    @Test
    void shouldCreateUpdatedBookDto() {
        var updatedTicket = new UpdatedTicketsDto();

        updatedTicket.setId(1);
        updatedTicket.setTitle("Titulo de teste");
        updatedTicket.setDescription("Descricao de teste");
        updatedTicket.setNameIssuer("Roger");
        updatedTicket.setEmailIssuer("roger@gmail.com");
        updatedTicket.setNameResponder("Marco");
        updatedTicket.setEmailResponder("marco@gmail.com");

        var result = ticketsBuilder.from(updatedTicket);

        assertEquals(updatedTicket.getId(), result.getId());
        assertEquals(updatedTicket.getTitle(), result.getTitle());
        assertEquals(updatedTicket.getDescription(), result.getDescription());
        assertEquals(updatedTicket.getNameIssuer(), result.getNameIssuer());
        assertEquals(updatedTicket.getEmailIssuer(), result.getEmailIssuer());
        assertEquals(updatedTicket.getNameResponder(), result.getNameResponder());
        assertEquals(updatedTicket.getEmailResponder(), result.getEmailResponder());
    }

    @Test
    void shouldCreateListOfTicketDto() {
        var ticket = Arrays.asList(mock(Tickets.class), mock(Tickets.class), mock(Tickets.class));

        var result = ticketsBuilder.from(ticket);

        assertNotNull(result);
        assertEquals(3, result.size());
    }


}
