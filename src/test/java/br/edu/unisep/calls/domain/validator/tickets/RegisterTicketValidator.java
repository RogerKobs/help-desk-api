package br.edu.unisep.calls.domain.validator.tickets;

import br.edu.unisep.calls.domain.dto.RegisterTicketsDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

import static br.edu.unisep.calls.domain.validator.ValidationMessages.*;

class RegisterTicketValidator {

    public RegisterTicketsValidator registerTicketsValidator;

    @BeforeEach
    void setup() {
        this.registerTicketsValidator = new RegisterTicketsValidator();
    }

    @Test
    void shouldValidatorTitleNotNull() {
        var registerTicket = new RegisterTicketsDto(null,
                "Descricao teste",
                "Roger",
                "roger@gmail.com",
                "Marco",
                "marco@gmail.com",
                null,
                null,
                null);

        assertThrows(NullPointerException.class, () -> registerTicketsValidator.validate(registerTicket), MESSAGE_REQUIRED_TICKETS_TITLE);
    }

    @Test
    void shouldValidatorTitleNotBlank() {
        var registerTicket = new RegisterTicketsDto("",
                "Descricao teste",
                "Roger",
                "roger@gmail.com",
                "Marco",
                "marco@gmail.com",
                null,
                null,
                null);

        assertThrows(IllegalArgumentException.class, () -> registerTicketsValidator.validate(registerTicket), MESSAGE_REQUIRED_TICKETS_TITLE);
    }

    @Test
    void shouldValidatorDescriptionNotNull() {
        var registerTicket = new RegisterTicketsDto("titulo de teste",
                null,
                "Roger",
                "roger@gmail.com",
                "Marco",
                "marco@gmail.com",
                null,
                null,
                null);

        assertThrows(NullPointerException.class, () -> registerTicketsValidator.validate(registerTicket), MESSAGE_REQUIRED_TICKETS_DESCRIPTION);
    }

    @Test
    void shouldValidatorDescriptionNotBlank() {
        var registerTicket = new RegisterTicketsDto("Titulo de teste",
                "",
                "Roger",
                "roger@gmail.com",
                "Marco",
                "marco@gmail.com",
                null,
                null,
                null);

        assertThrows(IllegalArgumentException.class, () -> registerTicketsValidator.validate(registerTicket), MESSAGE_REQUIRED_TICKETS_DESCRIPTION);
    }

}
