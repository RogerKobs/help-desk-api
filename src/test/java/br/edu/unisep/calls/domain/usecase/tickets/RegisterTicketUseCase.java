package br.edu.unisep.calls.domain.usecase.tickets;

import br.edu.unisep.calls.data.entity.Tickets;
import br.edu.unisep.calls.data.repository.TicketsRepository;
import br.edu.unisep.calls.domain.builder.TicketsBuilder;
import br.edu.unisep.calls.domain.dto.RegisterTicketsDto;
import br.edu.unisep.calls.domain.usecase.RegisterTicketsUseCase;
import br.edu.unisep.calls.domain.validator.tickets.RegisterTicketsValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

class RegisterTicketUseCase {

    private RegisterTicketsUseCase useCase;

    private TicketsBuilder builder;
    private RegisterTicketsValidator validator;
    private TicketsRepository repository;


    @BeforeEach
    void setup() {
        this.validator = mock(RegisterTicketsValidator.class);
        this.builder = mock(TicketsBuilder.class);
        this.repository = mock(TicketsRepository.class);

        this.useCase = new RegisterTicketsUseCase(validator, builder, repository);
    }

    @Test
    void shouldFindBookInvalid() {
        var registerTicket = mock(RegisterTicketsDto.class);
        doThrow(new IllegalArgumentException("Mock message")).when(validator).validate(any());

        assertThrows(IllegalArgumentException.class, () -> useCase.execute(registerTicket), "Mock message");

        verify(builder, times(0)).from(any(RegisterTicketsDto.class));
        verify(repository, times(0)).save(any(Tickets.class));
    }

    @Test
    void shouldSaveTicket() {
        var registerTicket = mock(RegisterTicketsDto.class);

        when(builder.from(any(RegisterTicketsDto.class))).thenReturn(mock(Tickets.class));

        useCase.execute(registerTicket);

        verify(validator, times(1)).validate(any(RegisterTicketsDto.class));
        verify(builder, times(1)).from(any(RegisterTicketsDto.class));
        verify(repository, times(1)).save(any(Tickets.class));
    }
}
