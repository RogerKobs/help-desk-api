package br.edu.unisep.calls.controller;

import br.edu.unisep.calls.data.entity.Tickets;
import br.edu.unisep.calls.domain.dto.RegisterTicketsDto;
import br.edu.unisep.calls.domain.dto.TicketsDto;
import br.edu.unisep.calls.domain.dto.UpdatedTicketsDto;
import br.edu.unisep.calls.domain.usecase.*;
import br.edu.unisep.calls.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/ticket")
public class TicketsController {

    private final RegisterTicketsUseCase registerTickets;
    private final UpdatedTicketsUseCase updatedTickets;
    private final FindTicketsByNameIssuerUseCase findByNameIssuer;
    private final FindTicketsByNameResponderUseCase findByNameResponder;
    private final FindAllTicketsUseCase findAllTickets;
    private final FindTicketsByIdUseCase findById;

    @GetMapping
    public ResponseEntity< DefaultResponse<List<TicketsDto>> > findAll() {
        var ticket = findAllTickets.execute();

        return ticket.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DefaultResponse.of(ticket));
    }

    @GetMapping("/{id}")
    public ResponseEntity<DefaultResponse<TicketsDto>> findById(@PathVariable("id") Integer id) {
        var book = findById.execute(id);

        return book == null ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(DefaultResponse.of(book));
    }

    @GetMapping("/byNameIssuer")
    public ResponseEntity< DefaultResponse<List<TicketsDto>> > findByNameIssuer(@RequestParam("nameIssuer") String nameIssuer) {
        var ticket = findByNameIssuer.execute(nameIssuer);

        return ticket == null ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(DefaultResponse.of(ticket));
    }

    @GetMapping("/byNameResponder")
    public ResponseEntity< DefaultResponse<List<TicketsDto>> > findByNameResponder(@RequestParam("nameResponder") String nameResponder) {
        var ticket = findByNameResponder.execute(nameResponder);

        return ticket == null ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(DefaultResponse.of(ticket));
    }

    @PostMapping
    public ResponseEntity<DefaultResponse<Boolean>> save(@RequestBody RegisterTicketsDto tickets) {
        registerTickets.execute(tickets);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }

}
