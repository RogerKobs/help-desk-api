package br.edu.unisep.calls.response;

import lombok.Getter;

@Getter
public class DefaultResponse<T> {

    private final String message;
    private final T result;

    private DefaultResponse(String message, T result) {
        this.message = message;
        this.result = result;
    }

    private DefaultResponse(T result) {
        this("", result);
    }

    public static <R> DefaultResponse<R> of(R result) {
        return new DefaultResponse<>(result);
    }

    public static <R> DefaultResponse<R> of(String message, R result) {
        return new DefaultResponse<>(message, result);
    }

}
