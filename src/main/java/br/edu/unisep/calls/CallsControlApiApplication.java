package br.edu.unisep.calls;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CallsControlApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CallsControlApiApplication.class, args);
	}

}
