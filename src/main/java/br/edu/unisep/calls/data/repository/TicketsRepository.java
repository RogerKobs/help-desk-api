package br.edu.unisep.calls.data.repository;

import br.edu.unisep.calls.data.entity.Tickets;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TicketsRepository extends JpaRepository<Tickets, Integer> {

    List<Tickets> findByNameIssuer(String nameIssuer);

    List<Tickets> findByNameResponder(String nameResponder);

}
