package br.edu.unisep.calls.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "tickets")
public class Tickets {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ticket_id")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "name_issuer")
    private String nameIssuer;

    @Column(name = "email_issuer")
    private String emailIssuer;

    @Column(name = "name_responder")
    private String nameResponder;

    @Column(name = "email_responder")
    private String emailResponder;

    @Column(name = "open_date")
    private LocalDate openDate;

    @Column(name = "status")
    private Integer status;

    @Column(name = "close_date")
    private LocalDate closeDate;

}
