package br.edu.unisep.calls.domain.validator.tickets;

import br.edu.unisep.calls.domain.dto.RegisterTicketsDto;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.calls.domain.validator.ValidationMessages.*;

@Component
public class RegisterTicketsValidator {

    public void validate(RegisterTicketsDto registerTickets) {

        Validate.notBlank(registerTickets.getTitle(), MESSAGE_REQUIRED_TICKETS_TITLE);
        Validate.notBlank(registerTickets.getDescription(), MESSAGE_REQUIRED_TICKETS_DESCRIPTION);
        Validate.notBlank(registerTickets.getNameIssuer(), MESSAGE_REQUIRED_TICKETS_NAME_ISSUER);
        Validate.notBlank(registerTickets.getNameResponder(), MESSAGE_REQUIRED_TICKETS_NAME_RESPONDER);
        Validate.notBlank(registerTickets.getEmailIssuer(), MESSAGE_REQUIRED_TICKETS_EMAIL_ISSUER);
        Validate.notBlank(registerTickets.getEmailResponder(), MESSAGE_REQUIRED_TICKETS_EMAIL_RESPONDER);

    }

}
