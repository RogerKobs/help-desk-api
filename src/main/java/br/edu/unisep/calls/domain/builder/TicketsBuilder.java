package br.edu.unisep.calls.domain.builder;

import br.edu.unisep.calls.data.entity.Tickets;
import br.edu.unisep.calls.domain.dto.RegisterTicketsDto;
import br.edu.unisep.calls.domain.dto.TicketsDto;
import br.edu.unisep.calls.domain.dto.UpdatedTicketsDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TicketsBuilder {

    public List<TicketsDto> from(List<Tickets> tickets) { return tickets.stream().map(this::from).collect(Collectors.toList()); }

    public TicketsDto from(Tickets tickets) {
        return new TicketsDto(
            tickets.getId(),
            tickets.getTitle(),
            tickets.getDescription(),
            tickets.getNameIssuer(),
            tickets.getEmailIssuer(),
            tickets.getNameResponder(),
            tickets.getEmailResponder(),
            tickets.getOpenDate(),
            tickets.getStatus(),
            tickets.getCloseDate()
        );
    }

    public Tickets from(UpdatedTicketsDto updatedTickets) {

        Tickets tickets = from((RegisterTicketsDto)  updatedTickets);
        tickets.setId(updatedTickets.getId());

        return tickets;
    }

    public Tickets from(RegisterTicketsDto registerTickets) {
        Tickets tickets = new Tickets();
        tickets.setTitle(registerTickets.getTitle());
        tickets.setDescription(registerTickets.getDescription());
        tickets.setNameIssuer(registerTickets.getNameIssuer());
        tickets.setEmailIssuer(registerTickets.getEmailIssuer());
        tickets.setNameResponder(registerTickets.getNameResponder());
        tickets.setEmailResponder(registerTickets.getEmailResponder());
        tickets.setOpenDate(registerTickets.getOpenDate());
        tickets.setStatus(registerTickets.getStatus());

        return tickets;
    }

}
