package br.edu.unisep.calls.domain.usecase;

import br.edu.unisep.calls.data.repository.TicketsRepository;
import br.edu.unisep.calls.domain.builder.TicketsBuilder;
import br.edu.unisep.calls.domain.dto.RegisterTicketsDto;
import br.edu.unisep.calls.domain.validator.tickets.RegisterTicketsValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@AllArgsConstructor
public class RegisterTicketsUseCase {

    private final RegisterTicketsValidator validator;
    private final TicketsBuilder builder;
    private final TicketsRepository repository;

    public void execute(RegisterTicketsDto registerTickets) {

        validator.validate(registerTickets);

        registerTickets.setOpenDate(LocalDate.now());
        registerTickets.setStatus(1);

        var ticket = builder.from(registerTickets);
        repository.save(ticket);
    }


}
