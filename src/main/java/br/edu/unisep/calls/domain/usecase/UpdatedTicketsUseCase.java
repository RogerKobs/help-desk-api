package br.edu.unisep.calls.domain.usecase;

import br.edu.unisep.calls.data.repository.TicketsRepository;
import br.edu.unisep.calls.domain.builder.TicketsBuilder;
import br.edu.unisep.calls.domain.dto.UpdatedTicketsDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UpdatedTicketsUseCase {

    private final TicketsBuilder builder;
    private final TicketsRepository repository;

    public void execute(UpdatedTicketsDto updatedTickets) {
        var ticked = builder.from(updatedTickets);
        repository.save(ticked);
    }

}
