package br.edu.unisep.calls.domain.dto;

import lombok.Data;

@Data
public class UpdatedTicketsDto extends RegisterTicketsDto {

    private Integer id;

}
