package br.edu.unisep.calls.domain.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationMessages {

    public static final String MESSAGE_REQUIRED_TICKETS_ID = "Informe o id do chamado";
    public static final String MESSAGE_REQUIRED_TICKETS_TITLE = "Informe o título do chamado!";
    public static final String MESSAGE_REQUIRED_TICKETS_DESCRIPTION = "Informe a descrição do chamado!";
    public static final String MESSAGE_REQUIRED_TICKETS_NAME_ISSUER = "Informe o nome do emissor!";
    public static final String MESSAGE_REQUIRED_TICKETS_EMAIL_ISSUER = "Informe o e-mail do emissor!";
    public static final String MESSAGE_REQUIRED_TICKETS_NAME_RESPONDER = "Informe o nome do respondedor!";
    public static final String MESSAGE_REQUIRED_TICKETS_EMAIL_RESPONDER = "Informe o e-mail do respondedor!";
    public static final String MESSAGE_REQUIRED_TICKETS_STATUS = "Informe o status do chamado!";

}
