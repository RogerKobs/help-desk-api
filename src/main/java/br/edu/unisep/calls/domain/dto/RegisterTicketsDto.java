package br.edu.unisep.calls.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterTicketsDto {

    private String title;

    private String description;

    private String nameIssuer;

    private String emailIssuer;

    private String nameResponder;

    private String emailResponder;

    private LocalDate openDate;

    private Integer status;

    private LocalDate closeDate;

}
