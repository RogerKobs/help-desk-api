package br.edu.unisep.calls.domain.usecase;

import br.edu.unisep.calls.data.repository.TicketsRepository;
import br.edu.unisep.calls.domain.builder.TicketsBuilder;
import br.edu.unisep.calls.domain.dto.TicketsDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FindTicketsByIdUseCase {

    private final TicketsRepository ticketRepository;
    private final TicketsBuilder builder;

    public TicketsDto execute(Integer ticketId) {

        var tickets = ticketRepository.findById(ticketId);
        return tickets.map(builder::from).orElse(null);
    }

}
