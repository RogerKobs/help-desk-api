package br.edu.unisep.calls.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class TicketsDto {

    private Integer id;

    private String title;

    private String description;

    private String nameIssuer;

    private String emailIssuer;

    private String nameResponder;

    private String emailResponder;

    private LocalDate openDate;

    private Integer status;

    private LocalDate closeDate;

}
