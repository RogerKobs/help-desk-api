package br.edu.unisep.calls.domain.usecase;

import br.edu.unisep.calls.data.repository.TicketsRepository;
import br.edu.unisep.calls.domain.builder.TicketsBuilder;
import br.edu.unisep.calls.domain.dto.TicketsDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindTicketsByNameIssuerUseCase {

    private final TicketsRepository repository;
    private final TicketsBuilder builder;

    public List<TicketsDto> execute(String nameIssuer) {
        var ticket = repository.findByNameIssuer(nameIssuer);
        return builder.from(ticket);
    }

}
